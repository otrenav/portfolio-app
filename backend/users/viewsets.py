# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import permissions, viewsets
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from . import serializers
from . import constants
from . import models


USER_MODEL = get_user_model()


class NewUserViewSet(APIView):

    permission_classes = [permissions.AllowAny]

    def post(self, request):
        error = self.check_data(request.data)
        if error:
            return(Response({ 'status': 'error', 'message': error }))

        user = USER_MODEL.objects.create(
            username=request.data['username'],
            email=request.data['email']
        )
        user.set_password(request.data['password'])
        user.save()
        return(Response({
            'status': 'OK',
            'message': 'Revisa tu correo. ¡Bienvenido!'
        }))

    def check_data(self, request_data):
        #
        # TODO: Security measures
        #       - Length
        #       - Complexity
        #       - Email supplied
        #       - Type
        #       - No varias cuentas desde una misma IP
        #         - Cuando se llegó al límite de cuentas,
        #           flag de las cuentas que se crearon
        #
        email = request_data.get('email', False)
        username = request_data.get('username', False)
        if not email:
            return("Elige un correo electrónico.")
        if not username:
            return("Elige un usuario.")
        if not request_data.get('password', False):
            return("Elige una contraseña.")
        if USER_MODEL.objects.filter(username=username).exists():
            return("Ese usuario ya está registrado. Elige otro usuario.")
        if USER_MODEL.objects.filter(email=email).exists():
            return("Ese correo electrónico ya está registrado. Elige otro correo electrónico.")
        return(None)


class ProfileViewSet(APIView):

    def post(self, request):
        error = self.check_data(request.data)
        if error:
            return(Response({ 'status': 'error', 'message': error }))

        user = USER_MODEL.objects.get(id=request.data['user_ID'])
        profile = models.Profile.objects.get(user=user)

        for field in constants.USER_FIELDS:
            new_value = request.data['profile'][field]
            if (user._meta
                .get_field(field)
                .get_internal_type() in constants.NULLABLE_FIELDS and
                not new_value):
                new_value = None
            setattr(user, field, new_value)

        for field in constants.PROFILE_FIELDS:
            new_value = request.data['profile'][field]
            if (profile._meta
                .get_field(field)
                .get_internal_type() in constants.NULLABLE_FIELDS and
                not new_value):
                new_value = None
            setattr(profile, field, new_value)

        user.save()
        profile.save()

        return(Response({
            'status': 'OK',
            'message': 'Perfil actualizado. ¡Gracias!'
        }))

    def check_data(self, request_data):
        #
        # TODO: Security measures
        #       - Length
        #       - Complexity
        #       - Email supplied
        #       - Type
        #
        email = request_data['profile'].get('email', False)
        username = request_data['profile'].get('username', False)
        if not email:
            return("Elige un correo electrónico.")
        if not username:
            return("Elige un usuario.")
        if (USER_MODEL.objects
            .exclude(id=request_data['user_ID'])
            .filter(username=username)
            .exists()):
            return("Ese usuario ya está registrado. Elige otro usuario.")
        if (USER_MODEL.objects
            .exclude(id=request_data['user_ID'])
            .filter(email=email)
            .exists()):
            return("Ese correo electrónico ya está registrado. Elige otro correo electrónico.")
        return(None)


class GetUserInfoViewSet(APIView):

    permission_classes = [permissions.AllowAny]

    def get(self, request, username=None):
        user = USER_MODEL.objects.get(username=username)
        profile = models.Profile.objects.get(user=user)
        groups = user.groups.values_list('name', flat=True)
        return(Response({
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'username': user.username,
            'groups': groups,
            'birth_date': profile.birth_date,
            'postal_code': profile.postal_code,
            'monthly_income': profile.monthly_income,
            'financial_dependents': profile.financial_dependents,
            'expected_monthly_savings': profile.expected_monthly_savings
        }))


class UserViewSet(viewsets.ModelViewSet):
    queryset = USER_MODEL.objects.all()
    serializer_class = serializers.UserSerializer


