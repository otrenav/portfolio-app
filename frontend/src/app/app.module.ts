
import { BrowserModule }             from '@angular/platform-browser';
import { NgModule }                  from '@angular/core';
import { ReactiveFormsModule }       from '@angular/forms';
import { HttpModule }                from '@angular/http';

import { ChartsModule }              from 'ng2-charts/ng2-charts';
import { CookieService }             from 'angular2-cookie/services/cookies.service';

import { AppComponent }              from './app.component';
import { AppRoutingModule }          from './app-routing.module';
import { AdminComponent }            from './admin/admin.component';
import { UserComponent }             from './user/user.component';
import { UserService }               from './user/user.service';
import { NewUserComponent }          from './user/new-user/new-user.component';
import { NewUserService }            from './user/new-user/new-user.service';
import { ProfileComponent }          from './user/profile/profile.component';
import { InvestmentsComponent }      from './investments/investments.component';
import { InvestmentHeaderComponent } from './investments/investment/investment-header.component';
import { InvestmentBodyComponent }   from './investments/investment/investment-body.component';
import { NewInvestmentComponent }    from './investments/new-investment/new-investment.component';
import { PerformanceComponent }      from './investments/investment/performance/performance.component';
import { CompositionComponent }      from './investments/investment/composition/composition.component';
import { EventsComponent }           from './investments/investment/events/events.component';
import { TransferComponent }         from './investments/investment/transfer/transfer.component';
import { QualificationComponent }    from './investments/investment/qualification/qualification.component';
import { InvestmentsService }        from './investments/investments.service';
import { TransferService }           from './investments/investment/transfer/transfer.service';
import { NewInvestmentService }      from './investments/new-investment/new-investment.service';
import { SiteComponent }             from './site/site.component';
import { HowItWorksComponent }       from './site/how-it-works/how-it-works.component';
import { QuestionsComponent }        from './questions/questions.component';
import { ProfileService }            from './user/profile/profile.service';


@NgModule({
    declarations: [
        AppComponent,
        InvestmentsComponent,
        InvestmentHeaderComponent,
        InvestmentBodyComponent,
        NewInvestmentComponent,
        PerformanceComponent,
        QualificationComponent,
        CompositionComponent,
        TransferComponent,
        EventsComponent,
        AdminComponent,
        UserComponent,
        ProfileComponent,
        NewUserComponent,
        SiteComponent,
        HowItWorksComponent,
        QuestionsComponent
    ],
    imports: [
        HttpModule,
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        ChartsModule
    ],
    providers: [
        UserService,
        NewUserService,
        ProfileService,
        InvestmentsService,
        NewInvestmentService,
        TransferService,
        CookieService,
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
