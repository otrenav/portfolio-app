
import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit }     from '@angular/core';

import { UserService }        from './user/user.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    private activeSiteSection: string;
    private loggedIn: boolean = false;
    private protectedURLWords: string[] = [
        "inversion",
        "perfil",
        "ayuda",
        "admin"
    ];

    constructor(
        private router: Router,
        private userService: UserService
    ) {
        router.events.subscribe((event) => {
            if(event instanceof NavigationEnd ) {
                this.SiteURLActiveCheck(event);
                this.URLAuthorizationCheck(event);
            }
        });
    }

    ngOnInit(): void {
        this.userService.validateUser();
    }

    private URLAuthorizationCheck(event: NavigationEnd): void {
        if (this.requiresAuthorization(event)) {
            if (this.userService.validateUser()) {
                this.loggedIn = true;
            } else {
                this.loggedIn = false;
            }
        } else {
            this.loggedIn = false;
        }
    }

    private SiteURLActiveCheck(event: NavigationEnd): void {
        // NOTE: This URLs correspond to sections in the main site
        if (event.url.indexOf('#inicio') !== -1) {
            this.activeSiteSection = 'inicio';
        } else if (event.url.indexOf('#invierte') !== -1) {
            this.activeSiteSection = 'invierte';
        } else if (event.url.indexOf('#contacto') !== -1) {
            this.activeSiteSection = 'contacto';
        } else {
            this.activeSiteSection = '';
        }
    }

    private isSectionActive(section: string): boolean {
        return section === this.activeSiteSection;
    }

    private logout(): void {
        this.userService.logout();
    }

    private requiresAuthorization(event: any): boolean {
        for (let i = 0; i < this.protectedURLWords.length; i++) {
            if (event.url.indexOf(this.protectedURLWords[i]) !== -1) {
                return true;
            }
        }
        return false;
    }

    private currentUserIsAdmin(): boolean {
        return this.userService.currentUserIsAdmin();
    }
}
